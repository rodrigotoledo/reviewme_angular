import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-review-answer',
  templateUrl: './review-answer.component.html',
  styleUrls: ['./review-answer.component.css']
})
export class ReviewAnswerComponent implements OnInit {

  @Input() reviewAnswer: any;

  constructor() { }

  ngOnInit() {
  }

}
