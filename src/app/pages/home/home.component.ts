import { Component, OnInit, Input, Injectable } from '@angular/core';
// import { Http } from '@angular/http';

import { SERVER_API } from '../../shared/app.api';
import { HttpClient } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

@Injectable()
export class HomeComponent implements OnInit {

  @Input() lastEmployees: any;
  @Input() totalEmployees: any;
  @Input() sizeOfEmployees: Number;

  @Input() lastReviews: any;
  @Input() totalReviews: any;
  @Input() sizeOfReviews: Number;

  @Input() lastReviewAnswers: any;
  @Input() totalReviewAnswers: any;
  @Input() sizeOfReviewAnswers: Number;


  @Input() formReview: any;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.http.get(`${SERVER_API}/employees/last_updated`).subscribe(resp => {
      this.lastEmployees = resp;
    });

    this.http.get(`${SERVER_API}/employees`).subscribe(resp => {
      this.totalEmployees = resp;
      this.sizeOfEmployees = Object.keys(this.totalEmployees).length;
    });





    this.http.get(`${SERVER_API}/reviews/last_updated`).subscribe(resp => {
      this.lastReviews = resp;
    });

    this.http.get(`${SERVER_API}/reviews`).subscribe(resp => {
      this.totalReviews = resp;
      this.sizeOfReviews = Object.keys(this.totalReviews).length;
    });






    this.http.get(`${SERVER_API}/review_answers/last_updated`).subscribe(resp => {
      this.lastReviewAnswers = resp;
    });

    this.http.get(`${SERVER_API}/review_answers`).subscribe(resp => {
      this.totalReviewAnswers = resp;
      this.sizeOfReviewAnswers = Object.keys(this.totalReviewAnswers).length;
    });
  }


  saveFormReview(formReview: any) {
    return this.http.post(`${SERVER_API}/reviews`,
      {
        employee_id: formReview.value.employee,
        title: formReview.value.title
      }).subscribe(resp => {
      // this.lastReviews = resp;
    });
  }

  saveFormEmployee(formEmployee: any) {
    return this.http.post(`${SERVER_API}/employees`,
      {
        name: formEmployee.value.name
      }).subscribe(resp => {
      if (!formEmployee.value.review || 0 === formEmployee.value.review.length) {
        // this.http.post(`${SERVER_API}/employees`,
        // {
        //   name: formEmployee.value.review
        // }).subscribe(resp => {});
      }
    });
  }

}
